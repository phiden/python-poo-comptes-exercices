class Compte:
    def __init__(self, numeroCompte, nomProprietaire, solde):
        self.numeroCompte = numeroCompte
        self.nomProprietaire = nomProprietaire
        self.solde = solde

    def retrait(self, montant):
        self.solde += montant
        print("J'ai fait un retrait de ", -montant, "euros")
        self.afficherSolde()

    def versement(self, montant):
        self.solde += montant
        print("J'ai fait un versement de ", montant, "euros")
        self.afficherSolde()

    def afficherSolde(self):
        print ("Mon solde est: " + str(self.solde) + " euros")


class CompteCourant(Compte):
    def __init__(self, numeroCompte, nomProprietaire, solde, autorisationDecouvert, pourcentageAgios):
        super().__init__(numeroCompte, nomProprietaire, solde)
        self.__autorisationDecouvert = autorisationDecouvert
        self.__pourcentageAgios = pourcentageAgios

    def appliquerAgios(self):
        if self.solde < 0:
            self.solde += self.solde * self.__pourcentageAgios
            print("Mon solde après Agios est: " + str(self.solde) + " euros")


class CompteEpargne(Compte):
    def __init__(self, numeroCompte, nomProprietaire, solde, pourcentageInterets):
        super().__init__(numeroCompte, nomProprietaire, solde)
        self.__pourcentageInterets = pourcentageInterets

    def appliquerInterets(self):
        self.solde += self.solde * self.__pourcentageInterets
        print("Mon solde après avoir appliqué les intérêts est: " + str(self.solde) + " euros")

