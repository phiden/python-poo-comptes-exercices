from compte.Compte import *
import pickle
import os

if __name__ == '__main__':


    NUMERO_COMPTE = '123456XXXX'
    NOM_PROPRIETAIRE = 'TRAN Ngoc-Phi'
    SOLDE_COURANT = 100
    SOLDE_EPARGNE = 1000
    AUTORISATION_DECOUVERT = -500
    POURCENTAGE_AGIOS = 0.1
    POURCENTAGE_INTERET = 0.015

    MESSAGE_COMPTE = "1: Compte courant | 2: Compte d'épargne | 3: Quitter\n"
    MESSAGE_MONTANT = "Selectioner votre montant: "

    listFiles = os.listdir()
    print(listFiles)
    if ("monCompteCourant.pkl" in listFiles):
        with open("monCompteCourant.pkl", "rb") as file:
            monCompteCourant = pickle.load(file)
    else:
        monCompteCourant = CompteCourant(NUMERO_COMPTE, NOM_PROPRIETAIRE, SOLDE_COURANT, AUTORISATION_DECOUVERT,
                                         POURCENTAGE_AGIOS)

    if ("monCompteEpargne.pkl" in listFiles):
        with open("monCompteEpargne.pkl", "rb") as file:
            monCompteEpargne = pickle.load(file)
    else:
        monCompteEpargne = CompteEpargne(NUMERO_COMPTE, NOM_PROPRIETAIRE, SOLDE_EPARGNE, POURCENTAGE_INTERET)

    print ("Bienvenue à ma banque \nSélectionner votre compte:")

    while True:
        try:
            print("---------------------------------------------------")
            compte = int(input(MESSAGE_COMPTE))
            if (compte in [1, 2, 3]):
                break
            raise Exception
        except:
            print("Entrer un nombre: 1, 2 ou 3")

    while compte != 3:
        print ("Compte courant" if compte == 1 else "Compte d'épargne", "selectionné")

        if compte == 1:
            monCompteCourant.afficherSolde()
            while True:
                try:
                    montant = int(input(MESSAGE_MONTANT))
                    break
                except:
                    print("Valeur non valid. Entrer un nombre")

            if montant > 0:
                monCompteCourant.versement(montant)
            else:
                monCompteCourant.retrait(montant)
                monCompteCourant.appliquerAgios()

        if compte == 2:
            monCompteEpargne.afficherSolde()
            while True:
                try:
                    montant = int(input(MESSAGE_MONTANT))
                    break
                except:
                    print("Valeur non valid. Entrer un nombre")
            if montant > 0:
                monCompteEpargne.versement(montant)
                monCompteEpargne.appliquerInterets()
            else:
                monCompteEpargne.retrait(montant)
        print("---------------------------------------------------")
        compte = int(input(MESSAGE_COMPTE))

    print("Au revoir")

    with open("monCompteCourant.pkl", "wb") as file:
        pickle.dump(monCompteCourant, file)

    with open("monCompteEpargne.pkl", "wb") as file:
        pickle.dump(monCompteEpargne, file)









